#include "ahorcado.h"

char* ahorcado(char *palabra_secreta) {
  char *resultado;
  char estado_juego[LARGO_MAXIMO_PALABRA];
  int vidas = VIDAS_AHORCADO;

  iniciar_tablero(palabra_secreta, estado_juego);
  
  mostrar_tablero(estado_juego);
  mostrar_vidas(vidas);

  int continua = 1;
  int acierto = -1; // Vale 0 si se acierta una letra. -1 en caso contrario.
  char letra;

  while(!juego_ganado(palabra_secreta, estado_juego) && vidas > 0 && continua) {
    letra = pedir_letra();
    continua = juego_continua(letra);

    if (continua) {
      acierto = modificar_tablero(palabra_secreta, estado_juego, letra);
      vidas += acierto;
    }
    
    mostrar_tablero(estado_juego);
    mostrar_vidas(vidas);
  }
  
  int largo_mensaje = 0;
  if (vidas == 0) {
    return generar_mensaje_perdedor(MENSAJE_PERDEDOR);
  } else if (!continua) {
      return generar_mensaje_perdedor(MENSAJE_TERMINACION);
  }
  
  resultado = generar_mensaje_ganador(vidas);
  return resultado;
}

void iniciar_tablero(char palabra_secreta[], char estado_juego[]) {
  int largo_palabra = strlen(palabra_secreta);

  for (int i = 0; i < largo_palabra; i++) {
    estado_juego[i] = '-';
  }

  estado_juego[largo_palabra] = '\0';
}

void mostrar_tablero(char palabra_secreta[]) {
  system(limpiar);
  printf("\n<< %s >>\n\n", palabra_secreta);
}

void mostrar_vidas(int vidas) {
  printf("Cantidad de vidas restantes: %d\n", vidas);
}

char pedir_letra() {
  char letra;

  printf("Ingrese una letra para jugar (o 1 para abandonar): ");
  scanf(" %c", &letra); // IMPORTANTE: el espacio se utiliza para ignorar espacios y endlines
  printf("pedir letra: %c\n", letra);

  return letra;
}

int juego_continua(char letra) {
  return !(letra == '1');
}

int modificar_tablero(char palabra_secreta[], char estado_juego[], char letra) {

  int largo_palabra = strlen(palabra_secreta);
  int modificaciones = 0;

  for (int i = 0; i < largo_palabra; i++) {
    if (palabra_secreta[i] == letra) {
      estado_juego[i] = letra;
      modificaciones++;
    }
  }

  if (modificaciones) {
    return 0;
  }
  return -1;

}

int juego_ganado(char palabra_secreta[], char estado_juego[]) {
  return strcmp(palabra_secreta, estado_juego) == 0;
}

char* generar_mensaje_ganador(int vidas) {
  int largo_mensaje = strlen(MENSAJE_GANADOR1) + strlen(MENSAJE_GANADOR2) + 5;
  // 5 es por " <n> " donde n en el número de vidas
  char *resultado = malloc(sizeof(char) * (largo_mensaje + 1));

  sprintf(resultado, "%s <%d> %s", MENSAJE_GANADOR1, vidas, MENSAJE_GANADOR2);
  return resultado;
}

char* generar_mensaje_perdedor(char const *mensaje) {
  int largo_mensaje = strlen(MENSAJE_PERDEDOR);
  char *resultado = malloc(sizeof(char) * (largo_mensaje + 1));
  
  strcpy(resultado, MENSAJE_PERDEDOR);
  return resultado;
}