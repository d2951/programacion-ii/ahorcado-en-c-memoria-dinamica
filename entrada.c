#include "entrada.h"

FILE* abrir_archivo(const char nombre_archivo[], char modo[]) {
  FILE *archivo = fopen(nombre_archivo, modo);
  assert(archivo != NULL);
  
  return archivo;
}

char* elegir_palabra(const char *nombre_archivo){
  char **palabras = malloc(sizeof(char*) * CANT_MAX_PALABRAS);

  char *palabra_secreta;

  FILE *archivo_objeto = abrir_archivo(nombre_archivo, "r");
  int cant_palabras = leer_palabras(archivo_objeto, palabras);
  fclose(archivo_objeto);

  // mostrar_palabras(palabras, cant_palabras);

  int indice_palabra_secreta = numero_aleatorio(0, cant_palabras);
  int largo_palabra_secreta = strlen(palabras[indice_palabra_secreta]);
  palabra_secreta = malloc(sizeof(char) * (largo_palabra_secreta + 1));
  strcpy(palabra_secreta, palabras[indice_palabra_secreta]);

  liberar_palabras(palabras, CANT_MAX_PALABRAS);

  return palabra_secreta;
}

int leer_palabras(FILE *archivo_objeto, char **palabras) {
  char linea[LARGO_MAXIMO_LINEA];

  int cant_palabras = 0;
  int largo_linea = 0;
  for(int i = 0; EOF != fscanf(archivo_objeto, "%s\n", linea); i++) {
    largo_linea = strlen(linea);
    palabras[i] = malloc(sizeof(char) * (largo_linea + 1));
    strncpy(palabras[i], linea, largo_linea);
    palabras[i][largo_linea] = '\0';
    cant_palabras++;
  }

  return cant_palabras;
}

void liberar_palabras(char **palabras, int cant_palabras) {
  for (int i = 0; i < cant_palabras; free(palabras[i++]));
  free(palabras);
}

void mostrar_palabras(char **palabras, int cant_palabras) {
  for (int i = 0; i < cant_palabras; printf("%s\n", palabras[i++]));
}

int numero_aleatorio(int minimo, int maximo) {
  srand(time(NULL));

  int tamano_intervalo = minimo - maximo + 1;
  return (rand() % tamano_intervalo) + minimo;
}