#include "main.h"

int main(int argc, char const *argv[]) {
  
  char *palabra_secreta, *resultado;

  if (argc != 2) {
    printf("Uso del ejecutable: %s <file>\n", argv[0]);
    return 1;
  }

  palabra_secreta = elegir_palabra(argv[1]);
  resultado = ahorcado(palabra_secreta);
  printf("\nFin del ahorcado\n%s\n", resultado);
  printf("La palabra era: %s\n", palabra_secreta);

  free(palabra_secreta);
  free(resultado);

  return 0;
}